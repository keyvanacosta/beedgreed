﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeedInteraction : MonoBehaviour {

    public bool inCup = false;

    public bool onBoard = false;

    public bool onWait = false;

    public int state = 0;

    public Color myColor;

    public int type;

    public int lastClicked;

    public PinStandObject myPin;
    public CupController myCup;

    public Vector3 place;
    public Vector3 waitPlace;

    public Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        
    }

    // Use this for initialization
    void Start ()
    {
        
	}

    public void moveMeToPlace()
    {
        transform.position = place;
    }

    public void initPlace(Vector3 _v)
    {
        place = _v;
    }

    public void instartiate(int i)
    {
        //Debug.Log("go to there");
        rb.isKinematic = true;
        rb.useGravity = false;
        float tY = transform.position.y;
        rb.position = waitPlace;
        transform.Translate(new Vector3(i, 0,0));

    }

    public void moveMeWaitPlace(int i)
    {
        //Debug.Log("go to there");
        rb.isKinematic = true;
        rb.useGravity = false;
        float tY = transform.position.y;
        rb.position = waitPlace;
        transform.Translate(new Vector3(i, 0, 0));

    }

    private void OnMouseDown()
    {
        if (inCup == true)
        {
         //   Debug.Log("entered");
            onWait = true;
            inCup = false;
            myCup.selectAllType(type);
        }
               

    }

    private void OnCollisionEnter(Collision collision)
    {

        inCup = true;
        
    }

    // Update is called once per frame
    void Update () {
		
	}
}
