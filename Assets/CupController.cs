﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class CupController : MonoBehaviour {

    public GameObject pinStand;

    //public List<GameObject> gameShapes = new List<GameObject>();

    public GameObject[] gameShapes;

    public List<GameObject> beeds;
    public List<GameObject> selectedBeeds;
    public Vector3 popInPlace;

    public int typeCreate = 0;

    public Color currentColor = Color.blue;

    // Use this for initialization
    void Start()
    {
        popInPlace = transform.position + Vector3.up;
        popInPlace += Vector3.up;
    }

    public void setType(int type)
    {
        typeCreate = type;
    }

    public IEnumerator create()
    {
        yield return new WaitForSeconds(1);
      
        GameObject temp;
        print("created Shape0");
        temp = Instantiate(gameShapes[typeCreate], popInPlace, Quaternion.identity);
        temp.transform.Rotate(Random.RandomRange(0, 100.0f), Random.RandomRange(0, 100.0f), Random.RandomRange(0, 100.0f));
        temp.transform.position = popInPlace;
        temp.GetComponent<BeedInteraction>().place = popInPlace;
        print(popInPlace + " "+ temp.transform.position);

        addBeed(temp, typeCreate);
        
        //switch (typeCreate)
        //{
        //    case 0:
        //        print("created Shape0");
        //        print(popInPlace);
        //        temp = Instantiate(gameShapes[typeCreate], popInPlace, Quaternion.identity);
        //        temp.transform.position = popInPlace;
        //        addBeed(temp);
        //        break;
        //    case 1:
        //        print("created Shape1");
        //        print(popInPlace);
        //        temp = Instantiate(gameShapes[typeCreate], popInPlace, Quaternion.identity);
        //        temp.transform.position = popInPlace;
        //        addBeed(temp);
        //        break;
        //    default:
        //        break;
        //}
    }

    private void Awake()
    {
        if (gameShapes == null)
            return;

        if (pinStand == null)
            return;

        beeds = new List<GameObject>();
        selectedBeeds = new List<GameObject>();

        if (!pinStand.GetComponent<PinStandObject>().Initialized)
        {
            pinStand.GetComponent<PinStandObject>().setShapes(gameShapes);
        }
    }

    public void addBeed(GameObject _beed, int _type = 0)
    {
        print(_beed);
        _beed.GetComponent<BeedInteraction>().myCup = this;
        _beed.GetComponent<BeedInteraction>().type = _type;
        beeds.Add(_beed);
    }

    public void moveSelectedToCup()
    {
        if (selectedBeeds.Count < 1)
        {
            //print("returned");
            return;
        }
        
        beeds.Clear();
        for(int i = 0; i < selectedBeeds.Count; i++)
        {

            beeds.Add (selectedBeeds[i]);

        }
        selectedBeeds.Clear();
        //print("beeds Count:" + beeds.Count.ToString());
    }

    public void selectAllType(int _type)
    {
        
        selectedBeeds.Clear();


        for (int i = 0; i < beeds.Count; i++)
        {
            print("bcount" + beeds.Count.ToString());
            //Debug.Log("OName:" + go.name);
            BeedInteraction myData = beeds[i].GetComponent<BeedInteraction>();
            if (myData != null)
            {
                if (myData.type == _type)
                {
                    myData.onWait = true;
                    myData.moveMeWaitPlace(i);
                    Debug.Log(myData);
                    selectedBeeds.Add(beeds[i]);
                    //                beeds.Remove(go);
                }
            }

        }

        for (int i = 0; i < beeds.Count; i++)
        {
            BeedInteraction myData = beeds[i].GetComponent<BeedInteraction>();
            if (myData.onWait == true)
            {
                beeds.Remove(beeds[i]);
              //  Debug.Log("Remove:" + go.name);
                if (beeds.Count < 1)
                    break;
            }

        }


        Debug.Log("call setType" + _type.ToString());
        pinStand.GetComponent<PinStandObject>().setShape(_type);

        foreach (GameObject go in selectedBeeds)
        {
            BeedInteraction myData = go.GetComponent<BeedInteraction>();
            go.transform.position = myData.waitPlace;

        }

        foreach (GameObject go in selectedBeeds)
        {
            BeedInteraction myData = go.GetComponent<BeedInteraction>();
            myData.rb.useGravity = true;
            myData.rb.isKinematic = false;

        }
        
    }

    public void removeOneFromSelected()
    {

        if (selectedBeeds.Count < 1)
        {
            
            pinStand.GetComponent<PinStandObject>().setShape(-1);
            selectedBeeds.Clear();
            beeds.Clear();
            return;           
        }

        GameObject GO;
        GO = selectedBeeds[0];
        selectedBeeds.RemoveAt(0);

        Destroy(GO);

    }

	
	// Update is called once per frame
	void Update () {
		
	}
}
