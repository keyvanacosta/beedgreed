﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatorScript : MonoBehaviour {

    public CupController cup;

    public int type;
	
    public IEnumerator create()
    {
        yield return new WaitForSeconds(.1f);

        GameObject temp;

        switch (type)
        {
            case 0:
                print(cup.popInPlace);
                temp = Instantiate(cup.gameShapes[0],cup.popInPlace, Quaternion.identity);
                temp.transform.position = cup.popInPlace;
                cup.addBeed(temp);
                break;
            default:
                break;
        }
    }


    //private void OnMouseDown()
    //{
    //    if (isCoroutine == true)
    //    {
    //        StartCoroutine("create");
    //    }
    //    else
    //    {

    //    }
    //}

    // Update is called once per frame
    void Update () {
		
	}
}
