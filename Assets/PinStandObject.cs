﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinStandObject : MonoBehaviour {

    static List<GameObject> shapes;

    static bool initialized = false;

    private bool unfilled = true;

    public bool occupied = false;

    static int currentShapeType = -1;
    public int currentShapeTypeViewer = -1;

    public bool Initialized
    {
        get
        {
            return initialized;
        }
        set
        {
            initialized = value;
        }
    }

    private void Awake()
    {
        shapes = new List<GameObject>();
    }

    public void setShapes(GameObject[] _shapes)
    //public void setShapes(List<GameObject> _shapes)
    {
        if (initialized)
            return;

        for (int i = 0; i < _shapes.Length; i++)
        {
            shapes.Add(_shapes[i]);
        }

    }

    //private void OnMouseDown()
    //{

    //    if (unfilled == true)
    //    {
    //        Instantiate(shapes[Random.RandomRange(0, shapes.Count)], gameObject.transform);
    //        unfilled = false;
    //    }
    //}

    private void OnMouseDown()
    {

        placeShape();
        //if (unfilled == true)
        //{
        //    Instantiate(shapes[Random.RandomRange(0, shapes.Count)], gameObject.transform);
        //    unfilled = false;
        //}
    }

    void placeShape()
    {
        if (currentShapeType == -1)
            return;

        if (unfilled == true)
        {
            
            GameObject GO = Instantiate(shapes[currentShapeType], gameObject.transform);
            BeedInteraction myData = GO.GetComponent<BeedInteraction>();
            myData.rb.isKinematic = true;
            myData.rb.useGravity = false;
            myData.initPlace(gameObject.transform.position);

            myData.moveMeToPlace();
            
            unfilled = false;
        }
    }

    public void setShape(int _shapeType)
    {
        currentShapeType = _shapeType;
        currentShapeTypeViewer = currentShapeType;
        Debug.Log("call setType" + _shapeType.ToString() + ":" + currentShapeType.ToString());
    }

    // Use this for initialization
    void Start () {

        
        //Instantiate(shapes[Random.RandomRange(0, shapes.Count)],gameObject.transform);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
