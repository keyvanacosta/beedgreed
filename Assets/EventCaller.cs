﻿//USE NAME OF FILE: "EventCaller.cs"
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class DoEvent
{
    public bool doEvent = true;
    public bool doneEvent = false;
    public bool doOnce = false;
    public UnityEvent whatEvent = null;

    public DoEvent()
    {
        doEvent = true;
        doneEvent = false;
        doOnce = false;
        whatEvent = null;
    }
}

public class EventCaller : MonoBehaviour
{

    public DoEvent eventToDo;
    public bool MouseEvent = true;
    public bool KeyEvent = false;
    public KeyCode Key = KeyCode.None;
    
    private IEnumerator OnMouseDown()
    {
        if (MouseEvent == true)
        {
            if (eventToDo.doneEvent == false)
            {
                if (eventToDo.whatEvent != null)
                {
                    eventToDo.whatEvent.Invoke();

                    if (eventToDo.doOnce == true)
                        eventToDo.doneEvent = true;
                }
            }
        }
        yield return null;
    }

    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (eventToDo.doneEvent == false)
            {
                if (eventToDo.whatEvent != null)
                {
                    eventToDo.whatEvent.Invoke();
                    if (eventToDo.doOnce == true)
                        eventToDo.doneEvent = true;
                }
            }
        }

    }

}